### Android Studio Layouts, Constraints, and Recyclerview ###

### Subject: ###
CIS2250

### Creation Date: ###
January 17th, 2022

### Authors: ###
Jesse Ford
Karina Akramov
Dakota Sawyer

### What is the purpose of this repository? ###

This repository is to serve as a method for easy access to the three user guides on how to use layouts, layout constraints, and recyclerview.

