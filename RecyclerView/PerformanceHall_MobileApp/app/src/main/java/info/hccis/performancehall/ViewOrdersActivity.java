package info.hccis.performancehall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

import info.hccis.performancehall.bo.CisUtility;

public class ViewOrdersActivity extends AppCompatActivity {

    //static attributes
    private static ArrayList<String> orderCosts = new ArrayList();
    private static double totalCost = 0;
    private static RecyclerView RecyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_orders);
        RecyclerAdapter = findViewById(R.id.recyclerView);

        Intent intent = getIntent();
        double cost = intent.getDoubleExtra(MainActivity.EXTRA_COST, 0);
        totalCost += cost;

        orderCosts.add(CisUtility.getCurrentDate("yyyy-MM-dd hh:mm:ss")+": "+cost);

        setAdapter();
    }
    // Access Adapter for the RecyclerView
    private void setAdapter(){
        RecyclerAdapter adapter = new RecyclerAdapter(orderCosts);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerAdapter.setLayoutManager(layoutManager);
        RecyclerAdapter.setItemAnimator(new DefaultItemAnimator());
        RecyclerAdapter.setAdapter(adapter);
    }

    public static String toStringOrders(){
        String output = "Cost details:"+System.lineSeparator();
        for(String current: orderCosts){
            output += current+System.lineSeparator();
        }
        output += "Total: $"+totalCost;
        return output;
    }

}