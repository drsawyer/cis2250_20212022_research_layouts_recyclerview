package info.hccis.performancehall;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;



public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

        private ArrayList<String> orderList;
        // Gets ArrayList
        public RecyclerAdapter(ArrayList<String> orderList){
            this.orderList = orderList;
        }
        // Sets up to change the text view on each iteration
        public class MyViewHolder extends RecyclerView.ViewHolder{
            private TextView txt;

            public MyViewHolder (final View view){
                super(view);
                txt = view.findViewById(R.id.textView);
            }
        }
        // Override RecyclerView with our own content
        @NonNull
        @Override
        public RecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view, parent, false);
            return new MyViewHolder(itemView);
        }
        // Override RecyclerView with our own content
        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter.MyViewHolder holder, int position) {
            String name = orderList.get(position);
            holder.txt.setText(name);
            System.out.println(orderList);

        }
        // Override RecyclerView with our own content
        @Override
        public int getItemCount() {
            return orderList.size();
        }
    }

