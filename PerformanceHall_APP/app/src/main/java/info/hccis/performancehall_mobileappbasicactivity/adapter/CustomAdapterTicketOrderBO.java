package info.hccis.performancehall_mobileappbasicactivity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import info.hccis.performancehall_mobileappbasicactivity.R;
import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderBO;

public class CustomAdapterTicketOrderBO extends RecyclerView.Adapter<CustomAdapterTicketOrderBO.TicketOrderBOViewHolder> {

    private ArrayList<TicketOrderBO> ticketOrderBOArrayList;

    public CustomAdapterTicketOrderBO(ArrayList<TicketOrderBO> ticketOrderBOArrayList) {
        this.ticketOrderBOArrayList = ticketOrderBOArrayList;
    }

    ;

    @NonNull
    @Override
    public CustomAdapterTicketOrderBO.TicketOrderBOViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_order, parent, false);
        return new TicketOrderBOViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterTicketOrderBO.TicketOrderBOViewHolder holder, int position) {

        String numberOfTickets = "" + ticketOrderBOArrayList.get(position).getNumberOfTickets();
        String hollpass = "" + ticketOrderBOArrayList.get(position).getHollPassNumber();
        String cost = "" + ticketOrderBOArrayList.get(position).getCost();
        holder.textViewNumberOfTickets.setText(numberOfTickets);
        holder.textViewHollPass.setText(hollpass);
        holder.textViewCost.setText(cost);

    }

    @Override
    public int getItemCount() {
        return ticketOrderBOArrayList.size();
    }

    public class TicketOrderBOViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewNumberOfTickets;
        private TextView textViewHollPass;
        private TextView textViewCost;

        public TicketOrderBOViewHolder(final View view) {
            super(view);
            textViewNumberOfTickets = view.findViewById(R.id.textViewNumberOfTicketsListItem);
            textViewHollPass = view.findViewById(R.id.textViewHollpassListItem);
            textViewCost = view.findViewById(R.id.textViewCostListItem);
        }
    }


}
